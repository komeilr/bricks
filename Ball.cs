﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace Bricks
{
    class Ball
    {
        public float X { get; set; }
        public float Y { get; set; }
        public float XVelocity { get; set; }
        public float YVelocity { get; set; }
        public float Height { get; set; }
        public float Width { get; set; }
        public bool UseRotation { get; set; }
        public float ScreenWidth { get; set; }
        public float ScreenHeight { get; set; }
        public bool Visible { get; set; }
        public int Score { get; set; }
        public int bricksCleared { get; set; }
    }
}
